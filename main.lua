local curl = require "cURL"
-- local json = require "json"

vk = {}
vk.api_version = "5.28"


function vk.http_query(method, args)
	curl.easy
	{
	url = string.format("https://api.vk.com/method/%s&%s&access+token=%s&v=%s", method, args, vk.group_token, vk.api_version),
	httpheader =
		{
			"query...",
		},
		-- postfields = jsonString
	}
	:perform()
	:close()
end


function vk.param(first, second)
	return first.."="..second
end


-- http_query("account.unban?"..vk.param("owner_id", "123456"))


